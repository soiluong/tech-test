# Tech Test

## Introduction

This repository includes the coding challenge solution.

The following user requirements are:

- As a user, when I type in a company name I should be shown a list of matching companies.
- As a user, when I select a company from the list, I should be shown the details of the company.
- As a user, when I type in an ABN I should be shown the matching company.

I've decided to implement the first two given the fact that being able to search by a company name 
(and not by ABN) and showing the results has more value for the user. Additionally, I felt that
it was important to timebox this challenge to 6 hours given the other commitments I have.

## Prerequisites

- Node v10.16.3
- Yarn v1.13.0 or higher
- Redux DevTools (optional)

## On First Run:

1. Clone the repository
2. Install dependencies `yarn install`
3. Start the app `yarn start`

## Testing:
- Lint: `yarn lint`
- Static Type: `yarn flow`
- Tests: `yarn test`

## Notes:
- Given the size of the tech challenge I still wanted to demonstrate how I would approach
handling Restful API calls, transform the response and storing the response in a normalised structure.
This will allow me to create memoized selectors, easily create/read/update/delete assets if it was
in a map object as shown in the Redux store without having many nested object levels.

- I would have liked to have properly typed everything and have more tests. But it was important to 
have the integration test to ensure my GET request returns the transformed response.

- I could have used `useReducer` instead of Redux to keep this project much simpler and just
documented/illustrated the store design and structure in this readme.
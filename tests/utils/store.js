import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import type { Api } from '../../src/types/Api';

export function createMockStoreWithApi(api: Api, mockedStore: Object = {}) {
  const middlewares = [thunk.withExtraArgument({ api })];
  const mockStore = configureMockStore(middlewares);
  return mockStore(mockedStore);
}

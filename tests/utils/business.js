export const mockBusiness = {
  abn: '123',
  abnStatus: 'status',
  isCurrent: true,
  name: 'Name',
  nameType: 'Business Name',
  postCode: '1234',
  score: 100,
  tate: 'NSW'
};

export const mockGetBusinessesApiResponse = {
  Message: '',
  Names: [
    {
      Abn: '69972259783',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: '@ ENGINEERING',
      NameType: 'Trading Name',
      Postcode: '2428',
      Score: 100,
      State: 'NSW'
    },
    {
      Abn: '69896109400',
      AbnStatus: '0000000001',
      IsCurrent: true,
      Name: 'AKA ENGINEERING',
      NameType: 'Trading Name',
      Postcode: '6076',
      Score: 100,
      State: 'WA'
    },
    {
      Abn: '50097870703',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: 'AKA ENGINEERING',
      NameType: 'Business Name',
      Postcode: '6000',
      Score: 100,
      State: 'WA'
    },
    {
      Abn: '53738378173',
      AbnStatus: '0000000001',
      IsCurrent: true,
      Name: 'CO ENGINEERING',
      NameType: 'Trading Name',
      Postcode: '2168',
      Score: 100,
      State: 'NSW'
    },
    {
      Abn: '69484963913',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: 'ENGINEER',
      NameType: 'Trading Name',
      Postcode: '3195',
      Score: 100,
      State: 'VIC'
    },
    {
      Abn: '52896189059',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: 'ENGINEERING',
      NameType: 'Entity Name',
      Postcode: '2198',
      Score: 100,
      State: 'NSW'
    },
    {
      Abn: '40007480446',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: 'ENGINEERING',
      NameType: 'Other Name',
      Postcode: '4818',
      Score: 100,
      State: 'QLD'
    },
    {
      Abn: '67330738613',
      AbnStatus: '0000000001',
      IsCurrent: true,
      Name: 'ENGINEERING',
      NameType: 'Trading Name',
      Postcode: '3931',
      Score: 100,
      State: 'VIC'
    },
    {
      Abn: '45361736782',
      AbnStatus: '0000000001',
      IsCurrent: true,
      Name: 'ENGINEERING',
      NameType: 'Trading Name',
      Postcode: '3169',
      Score: 100,
      State: 'VIC'
    },
    {
      Abn: '31081598138',
      AbnStatus: '0000000002',
      IsCurrent: true,
      Name: 'ENGINEERING PTY LTD',
      NameType: 'Trading Name',
      Postcode: '4280',
      Score: 100,
      State: 'QLD'
    }
  ]
};

export const transformedMockGetBusinessesApiResponse = {
  listIds: [
    '69972259783',
    '69896109400',
    '50097870703',
    '53738378173',
    '69484963913',
    '52896189059',
    '40007480446',
    '67330738613',
    '45361736782',
    '31081598138'
  ],
  resources: {
    '69972259783': {
      abn: '69972259783',
      abnStatus: '0000000002',
      isCurrent: true,
      name: '@ ENGINEERING',
      nameType: 'Trading Name',
      postCode: '2428',
      score: 100,
      state: 'NSW'
    },
    '69896109400': {
      abn: '69896109400',
      abnStatus: '0000000001',
      isCurrent: true,
      name: 'AKA ENGINEERING',
      nameType: 'Trading Name',
      postCode: '6076',
      score: 100,
      state: 'WA'
    },
    '50097870703': {
      abn: '50097870703',
      abnStatus: '0000000002',
      isCurrent: true,
      name: 'AKA ENGINEERING',
      nameType: 'Business Name',
      postCode: '6000',
      score: 100,
      state: 'WA'
    },
    '53738378173': {
      abn: '53738378173',
      abnStatus: '0000000001',
      isCurrent: true,
      name: 'CO ENGINEERING',
      nameType: 'Trading Name',
      postCode: '2168',
      score: 100,
      state: 'NSW'
    },
    '69484963913': {
      abn: '69484963913',
      abnStatus: '0000000002',
      isCurrent: true,
      name: 'ENGINEER',
      nameType: 'Trading Name',
      postCode: '3195',
      score: 100,
      state: 'VIC'
    },
    '52896189059': {
      abn: '52896189059',
      abnStatus: '0000000002',
      isCurrent: true,
      name: 'ENGINEERING',
      nameType: 'Entity Name',
      postCode: '2198',
      score: 100,
      state: 'NSW'
    },
    '40007480446': {
      abn: '40007480446',
      abnStatus: '0000000002',
      isCurrent: true,
      name: 'ENGINEERING',
      nameType: 'Other Name',
      postCode: '4818',
      score: 100,
      state: 'QLD'
    },
    '67330738613': {
      abn: '67330738613',
      abnStatus: '0000000001',
      isCurrent: true,
      name: 'ENGINEERING',
      nameType: 'Trading Name',
      postCode: '3931',
      score: 100,
      state: 'VIC'
    },
    '45361736782': {
      abn: '45361736782',
      abnStatus: '0000000001',
      isCurrent: true,
      name: 'ENGINEERING',
      nameType: 'Trading Name',
      postCode: '3169',
      score: 100,
      state: 'VIC'
    },
    '31081598138': {
      abn: '31081598138',
      abnStatus: '0000000002',
      isCurrent: true,
      name: 'ENGINEERING PTY LTD',
      nameType: 'Trading Name',
      postCode: '4280',
      score: 100,
      state: 'QLD'
    }
  }
};

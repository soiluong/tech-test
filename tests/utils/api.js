export default function mockApi(mockedResponses) {
  return {
    get: url => {
      return new Promise((resolve, reject) => {
        process.nextTick(() => {
          url
            ? resolve(mockedResponses.get)
            : reject({ error: 'No url found', status: 404 });
        });
      });
    }
  }
}
import styled from 'styled-components/macro';

export const BusinessHeading = styled.h3`
  margin: 0 0 10px;
`;

export const DescTag = styled.dt`
  font-weight: bold;
`;

export const DescDet = styled.dd`
  margin-bottom: 5px;
`;

import React from 'react';
import renderer from 'react-test-renderer';

import SelectedBusiness from '../index';
import { mockBusiness } from '../../../../tests/utils/business';

describe('SelectedBusiness component', () => {
  const component = (business = null) =>
    renderer.create(<SelectedBusiness business={business} />);

  it('should not render the component if no argument given', () => {
    let tree = component().toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('should render the component with business details', () => {
    let tree = component(mockBusiness).toJSON();
    expect(tree).toMatchSnapshot();
  });
});

// @flow
import React from 'react';

import { BusinessHeading, DescDet, DescTag } from './styled';

import type { TransformedBusiness } from 'types/Business';

type Props = {
  business: TransformedBusiness
};

export default function SelectedBusiness({ business }: Props) {
  if (!business) {
    return null;
  }

  const {
    abn,
    abnStatus,
    isCurrent,
    name,
    nameType,
    postCode,
    score,
    state
  } = business;

  return (
    <>
      <BusinessHeading>Showing {name}</BusinessHeading>
      <dl>
        <DescTag>Abn:</DescTag>
        <DescDet>{abn}</DescDet>
        <DescTag>Abn Staus:</DescTag>
        <DescDet>{abnStatus}</DescDet>
        <DescTag>Is Current:</DescTag>
        <DescDet>{isCurrent ? 'True' : 'False'}</DescDet>
        <DescTag>Name:</DescTag>
        <DescDet>{name}</DescDet>
        <DescTag>Name Type:</DescTag>
        <DescDet>{nameType}</DescDet>
        <DescTag>Post Code:</DescTag>
        <DescDet>{postCode}</DescDet>
        <DescTag>Score:</DescTag>
        <DescDet>{score}</DescDet>
        <DescTag>State:</DescTag>
        <DescDet>{state}</DescDet>
      </dl>
    </>
  );
}

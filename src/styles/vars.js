// @flow
import { css } from 'styled-components';

// Grid sizes
const colWidth = 14;
const colMarginWidth = 12;

// Calculators
const calcColsWithMargins = (numCols: number): number =>
  (colWidth + colMarginWidth) * numCols;
export const cols = (numCols: number): string =>
  `${calcColsWithMargins(numCols) - colMarginWidth}px`;

export const breakpoints = {
  large: cols(45),
  medium: cols(33),
  small: cols(20)
};

export const media = Object.keys(breakpoints).reduce(
  (acc: Object, label: string) => {
    acc[label] = (...args: Array<any>) => css`
      @media (max-width: ${breakpoints[label]}) {
        ${css(...args)};
      }
    `;

    return acc;
  },
  {}
);

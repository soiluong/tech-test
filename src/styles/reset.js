// @flow
export default `
html {
  box-sizing: border-box;
  font-size: 100%;
}

*, *:before, *:after {
  box-sizing: inherit;
}

a, p, span {
  word-wrap: break-word;
  line-height: 1.6;
}
`;

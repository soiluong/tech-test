// @flow
import { createGlobalStyle } from 'styled-components';

import normalize from './normalize';
import reset from './reset';

export default createGlobalStyle`
  ${normalize};
  ${reset};
  
  :root {
    --color-black: #000000;
    --color-white: #ffffff;
    --color-grey: #d8d8d8;
  }
  
  html.lock-scroll,
  html.lock-scroll body {
    height: 100%;
    overflow: hidden;
    position: fixed;
    width: 100%;
    -webkit-overflow-scrolling: touch;
  }
  
  body {
    background: var(--color-white);
    color: var(--color-black);
    font-family: 'Fira Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    -webkit-overflow-scrolling: touch;
  }
  
  legend {
    text-transform: capitalize;
  }
  
  h1, h2, h3, h4, h5, h6 {
    font-weight: bold;
    text-transform: uppercase;
  }
`;

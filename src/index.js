// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware, compose } from 'redux';
import { Provider as ReduxProvider } from 'react-redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import getApi from 'api/api';
import generateStateStore from 'state/generate-state-store';
import Homepage from 'pages/Homepage';

const rootElement = document.getElementById('root');
const api = getApi();
const store = generateStateStore(
  {},
  compose(
    composeWithDevTools(applyMiddleware(thunk.withExtraArgument({ api })))
  )
);

if (rootElement !== null) {
  ReactDOM.render(
    <ReduxProvider store={store}>
      <Homepage />
    </ReduxProvider>,
    rootElement
  );
}

import { combineReducers, createStore } from 'redux';

import { pipe } from 'utils';

import businesses from './reducers/businesses';
import lists from './reducers/lists';

const reducer = pipe(combineReducers)({
  businesses,
  lists
});

export default function generateStateStore(
  initialState: Object = undefined, // TODO: proper typing
  middleware: Function = undefined
) {
  return createStore(reducer, initialState, middleware);
}

// @flow
import { createReducerFromMap } from 'utils';
import {
  INVALIDATE_LIST,
  LIST_REQUESTED,
  LIST_UPDATED
} from 'state/actions/lists';

import type { State } from 'types/State';

function createList(props: Object = {}) {
  return {
    ids: [],
    isFetching: false,
    isInvalid: false,
    lastUpdated: props.timestamp || new Date(),
    ...props
  };
}

function listRequested(
  state: State,
  { id, timestamp }: { id: string, timestamp: string }
) {
  const newList = state[id] ? { ...state[id] } : createList({ timestamp });

  newList.isFetching = true;

  return {
    ...state,
    [id]: newList
  };
}

function updateList(state: State, id: string, props: Object) {
  if (state[id]) {
    return {
      ...state,
      [id]: {
        ...state[id],
        lastUpdated: new Date(),
        isFetching: false,
        isInvalid: false,
        ...props
      }
    };
  }

  return state;
}

function invalidateList(state: State, { id }: { id: string }) {
  return updateList(state, id, {
    isInvalid: true
  });
}

function validateActionTimestamp(func: Function) {
  return (state, action) => {
    if (action.timestamp && state[action.id].timestamp) {
      if (action.timestamp >= state[action.id].timestamp) {
        return func(state, action);
      }
      return { ...state };
    }
    return func(state, action);
  };
}

function getId(item) {
  return typeof item === 'string' ? item : item.id;
}

function listReceived(
  state: State,
  action: { id: string, items: string[], type: string }
) {
  const { id, items, type, ...props } = action;
  const list = state[id];
  const newResourceIds = typeof items !== 'undefined' ? items.map(getId) : [];
  const uniqueSet = Array.from(new Set([...newResourceIds]));
  const ids = [...uniqueSet];

  return {
    ...state,
    [id]: createList({
      ...list,
      lastUpdated: new Date(),
      ids,
      ...props
    })
  };
}

export default createReducerFromMap(
  {
    [LIST_REQUESTED]: listRequested,
    [INVALIDATE_LIST]: invalidateList,
    [LIST_UPDATED]: validateActionTimestamp(listReceived)
  },
  {}
);

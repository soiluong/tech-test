// @flow
import { assetsReducer } from 'utils';
import { businessActionTypes } from 'state/actions/businesses';

export default assetsReducer(businessActionTypes);

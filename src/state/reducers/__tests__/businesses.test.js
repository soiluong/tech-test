import reducer from '../businesses';
import { businessActionTypes } from '../../actions/businesses';
import { mockBusiness } from '../../../../tests/utils/business';

describe('businesses reducer', () => {
  it('should return the initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });

  it('should add businesses into the state', () => {
    const action = {
      type: businessActionTypes.MULTIPLE_RECEIVED,
      payload: {
        [mockBusiness.abn]: mockBusiness
      }
    };

    const expectedState = {
      [mockBusiness.abn]: mockBusiness
    };
    expect(reducer({}, action)).toEqual(expectedState);
  });
});

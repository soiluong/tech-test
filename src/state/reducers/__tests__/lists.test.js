import reducer from '../lists';
import {
  INVALIDATE_LIST,
  LIST_REQUESTED,
  LIST_UPDATED
} from '../../actions/lists';

describe('lists reducer', () => {
  it('should return the initial state', () => {
    expect(reducer({}, {})).toEqual({});
  });

  it('should add a new list', () => {
    const timestamp = new Date();

    const action = {
      type: LIST_REQUESTED,
      id: 'my-list',
      timestamp
    };

    const expectedState = {
      'my-list': {
        ids: [],
        isFetching: true,
        isInvalid: false,
        lastUpdated: timestamp,
        timestamp
      }
    };

    expect(reducer({}, action)).toEqual(expectedState);
  });

  it('should invalidate a list', () => {
    const action = {
      type: INVALIDATE_LIST,
      id: 'my-list'
    };

    const initialState = {
      'my-list': {
        ids: [],
        isFetching: false,
        isInvalid: false
      }
    };

    const expectedState = {
      'my-list': {
        ids: [],
        isFetching: false,
        isInvalid: true,
        lastUpdated: new Date()
      }
    };

    expect(reducer(initialState, action)).toEqual(expectedState);
  });

  it('should update a list', () => {
    const action = {
      type: LIST_UPDATED,
      id: 'my-list'
    };

    const initialState = {
      'my-list': {
        ids: [],
        isFetching: false,
        isInvalid: false
      }
    };

    const expectedState = {
      'my-list': {
        ids: [],
        isFetching: false,
        isInvalid: false,
        lastUpdated: new Date()
      }
    };

    expect(reducer(initialState, action)).toEqual(expectedState);
  });
});

import {
  INVALIDATE_LIST,
  invalidateList,
  LIST_REQUESTED,
  LIST_UPDATED,
  listRequested,
  listUpdated
} from '../lists';

describe('lists actions', () => {
  it('should create an action to request list', () => {
    const id = 'test-id';
    const timestamp = new Date();

    const expectedAction = {
      type: LIST_REQUESTED,
      id,
      timestamp
    };
    expect(listRequested(id, timestamp)).toEqual(expectedAction);
  });

  it('should create an action to invalidate list', () => {
    const id = 'test-id';

    const expectedAction = {
      type: INVALIDATE_LIST,
      id
    };
    expect(invalidateList(id)).toEqual(expectedAction);
  });

  it('should create an action to update list', () => {
    const id = 'test-id';
    const items = [];
    const timestamp = new Date();

    const expectedAction = {
      type: LIST_UPDATED,
      id,
      items,
      timestamp
    };

    expect(listUpdated({ id, items, timestamp })).toEqual(expectedAction);
  });
});

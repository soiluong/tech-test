import sinon from 'sinon';

import { createMockStoreWithApi } from '../../../../tests/utils/store';
import { searchBusinessesRequest } from '../businesses';
import { makeApiLink } from '../../../api/api';

const api = {
  get: sinon.spy(() => Promise.resolve())
};

describe('Business actions', () => {
  let store;

  beforeEach(() => {
    store = createMockStoreWithApi(api);
  });

  it('calls endpoint', () => {
    const searchTerm = 'hello world';
    store.dispatch(searchBusinessesRequest(searchTerm));

    expect(
      api.get.withArgs(makeApiLink(`name=${searchTerm}&maxResults=10`))
        .calledOnce
    ).toBe(true);
  });
});

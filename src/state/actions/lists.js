// @flow
export const LIST_REQUESTED = 'LIST_REQUESTED';
export const INVALIDATE_LIST = 'INVALIDATE_LIST';
export const LIST_UPDATED = 'LIST_UPDATED';

export function listRequested(id: string, timestamp: Date) {
  return { type: LIST_REQUESTED, id, timestamp };
}

export function invalidateList(id: string) {
  return { type: INVALIDATE_LIST, id };
}

export function listUpdated(list: {
  id: string,
  items: string[],
  timestamp: Date
}) {
  return { type: LIST_UPDATED, ...list };
}

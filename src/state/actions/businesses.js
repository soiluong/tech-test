// @flow
import { assetActionCreators, assetActionTypes } from 'utils';
import { getBusinesses as searchBusinessesFromApi } from 'api/businesses';
import { invalidateList, listRequested, listUpdated } from './lists';
import { BUSINESS_SEARCH_RESULT_LIST_ID } from 'constants/list-names';

import type { Dispatch, GetState } from 'types/Redux';
import type { Api } from 'types/Api';
import type {
  AssetActionCreators,
  AssetActionTypes
} from 'utils/create-asset-actions';

export const businessActionTypes: AssetActionTypes = assetActionTypes(
  'BUSINESS'
);
export const businessActions: AssetActionCreators = assetActionCreators(
  businessActionTypes
);

export function searchBusinessesRequest(term: string) {
  return function(
    dispatch: Dispatch,
    getState: GetState,
    { api }: { api: Api }
  ) {
    const listId = BUSINESS_SEARCH_RESULT_LIST_ID;
    const timestamp = new Date();
    dispatch(listRequested(listId, timestamp));

    return searchBusinessesFromApi(api)(term)
      .then(({ listIds, resources }) => {
        dispatch(invalidateList(listId));
        dispatch(listUpdated({ id: listId, items: listIds, timestamp }));
        dispatch(businessActions.multipleReceived(resources));
      })
      .catch(() => {
        businessActions.requestFailed(term);
      });
  };
}

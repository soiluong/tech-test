import { createSelector } from 'reselect';
import type { State } from 'types/State';

// TODO: proper typing
export function getLists({ lists }: { lists: Array<string> }) {
  return lists;
}

export function getList(state: State, id: string) {
  return getLists(state)[id];
}

export function mapItemsToIds(ids: string[], items: Object) {
  return ids && items ? ids.map(id => items[id]) : null;
}

export const getListItemIds = createSelector(
  getList,
  list => (list ? list.ids : null)
);

export const isListFetching = createSelector(
  getList,
  list => (list ? list.isFetching : null)
);

// @flow
import { createSelector } from 'reselect';

import { BUSINESS_SEARCH_RESULT_LIST_ID } from 'constants/list-names';
import { getListItemIds, isListFetching, mapItemsToIds } from './lists';

import type { State } from 'types/State';

export function getBusinesses(state: State) {
  return state.businesses;
}

export function getBusiness(state: State, id: string) {
  return getBusinesses(state)[id];
}

function getBusinessSearchResults(state: State) {
  return getListItemIds(state, BUSINESS_SEARCH_RESULT_LIST_ID);
}

export const getSearchedBusinesses = createSelector(
  getBusinessSearchResults,
  getBusinesses,
  mapItemsToIds
);

export function isBusinessSearchListFetching(state: State) {
  return isListFetching(state, BUSINESS_SEARCH_RESULT_LIST_ID);
}

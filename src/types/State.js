// @flow
import type { List } from './List';
import type { BusinessMap } from './Business';

type ListMap = {
  [string]: List
};

export type State = {
  businesses: BusinessMap,
  lists: ListMap
};

// @flow
export type Business = {
  Abn: string,
  AbnStatus: string,
  IsCurrent: boolean,
  Name: string,
  NameType: string,
  Postcode: string,
  Score: 100,
  State: string
};

export type TransformedBusiness = {
  abn: string,
  abnStatus: string,
  isCurrent: boolean,
  name: string,
  nameType: string,
  postCode: string,
  score: 100,
  state: string
};

export type BusinessMap = {
  [string]: TransformedBusiness
};

export type NormalisedMultipleBusinesses = {
  listIds: string[],
  resources: BusinessMap
};

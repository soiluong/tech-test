export type List = {
  lastUpdated: Date,
  ids: Array<string>,
  moreItemsUrl: string | false,
  limit?: number,
  isAcceptingNewItems: boolean,
  isAtBeginning?: boolean,
  isAtEnd?: boolean,
  numPendingItems: number,
  isFetching: boolean,
  isInvalid: boolean
};

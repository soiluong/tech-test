// @flow
export type Api = {
  get: (url: string, body?: {}) => Promise<*>
};

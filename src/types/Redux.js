// @flow
import type { Api } from './Api';
import type { State } from './State';

export type Dispatch = Function;
export type GetState = () => State;
export type ThunkArgs = {
  api: Api
};

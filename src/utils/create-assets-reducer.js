import { createReducerFromMap } from './create-reducer-from-map';

import type {
  ActionWithMultipleAssets,
  ActionWithGenericPayload,
  AssetActionTypes,
  ActionWithId
} from './create-asset-actions';

function addMultipleNewAssets(
  state: {},
  { payload }: ActionWithMultipleAssets
): {} {
  return {
    ...state,
    ...payload
  };
}

function updateAsset(state: {}, action: ActionWithGenericPayload): {} {
  const { id, payload } = action;
  return {
    ...state,
    [id]: {
      ...state[id],
      isFetching: false,
      isInvalid: false,
      ...payload
    }
  };
}

function assetUpdater(payload: {} | Function): Function {
  return (state: {}, action: ActionWithId): {} =>
    updateAsset(state, {
      id: action.id,
      payload:
        typeof payload === 'function' && state[action.id]
          ? payload(state[action.id], action)
          : payload
    });
}

export function assetsReducer(
  actionTypes: AssetActionTypes,
  reducers: {} = {}
): Function {
  return createReducerFromMap(
    {
      [actionTypes.MULTIPLE_RECEIVED]: addMultipleNewAssets,
      [actionTypes.REQUEST_FAILED]: assetUpdater({ isFetching: false }),
      ...reducers
    },
    {}
  );
}

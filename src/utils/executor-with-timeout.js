/**
 * @flow
 * Set time limit on how long an API call is executed
 */
export function executorWithTimeout(
  executor: Function,
  timeout: number,
  timeoutError: string
) {
  return function(resolve: Function, reject: Function) {
    const executorTimerId = setTimeout(() => reject(timeoutError), timeout);
    return executor(
      response => {
        clearTimeout(executorTimerId);
        resolve(response);
      },
      err => {
        clearTimeout(executorTimerId);
        reject(err);
      }
    );
  };
}

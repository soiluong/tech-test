// @flow
export function pipe(...fns: Function) {
  return (x: Function) => fns.reduce((v, f) => f(v), x);
}

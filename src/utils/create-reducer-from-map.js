// @flow
export function createReducerFromMap(
  map: Object,
  defaultState: Object // TODO: correct typing for this
) {
  return (state: Object, action: { type: string }) =>
    map[action.type] ? map[action.type](state, action) : state || defaultState;
}

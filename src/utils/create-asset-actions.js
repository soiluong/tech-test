// @flow
export type AssetActionTypes = {
  REQUESTED: string,
  RECEIVED: string,
  MULTIPLE_RECEIVED: string,
  REQUEST_FAILED: string,
  DELETE: string,
  UPDATE: string,
  INVALIDATE: string
};

export type AssetActionCreators = {
  requested: Function,
  received: Function,
  multipleReceived: Function,
  requestFailed: Function,
  update: Function,
  delete: Function,
  invalidate: Function
};

export type Asset = {
  id: string
};

export type AssetArray = Array<Asset>;

type Action = {
  type: string
};

export type ActionWithId = Action & {
  id: string
};

export type ActionWithMultipleAssets = Action & {
  payload: Array<Asset>
};

export type ActionWithGenericPayload = {
  id: string,
  payload: {}
};

export function assetActionTypes(type: string): AssetActionTypes {
  return {
    REQUESTED: `${type}_REQUESTED`,
    RECEIVED: `${type}_RECEIVED`,
    MULTIPLE_RECEIVED: `MULTIPLE_${type}S_RECEIVED`,
    REQUEST_FAILED: `${type}_REQUEST_FAILED`,
    DELETE: `DELETE_${type}`,
    UPDATE: `UPDATE_${type}`,
    INVALIDATE: `INVALIDATE_${type}`
  };
}

export function actionCreatorWithId(type: string): Function {
  return (id: string, payload: any): ActionWithId => ({ type, id, payload });
}

export function assetActionCreators(
  actionTypes: AssetActionTypes
): AssetActionCreators {
  return {
    requested: () => {}, // TODO or not...
    received: () => {}, // TODO or not...
    multipleReceived: (payload: AssetArray): ActionWithMultipleAssets => ({
      type: actionTypes.MULTIPLE_RECEIVED,
      payload
    }),
    requestFailed: actionCreatorWithId(actionTypes.REQUEST_FAILED),
    update: () => {}, // TODO or not...
    delete: () => {}, // TODO or not...
    invalidate: () => {} // TODO or not...
  };
}

// @flow
const CALLBACK = 'callback';
export function makeApiLink(path: string): string {
  /**
   * ideally this should be served from a backend API endpoint to obscure the guid
   */
  return `https://cors-anywhere.herokuapp.com/http://abr.business.gov.au/json/MatchingNames.aspx?callback=${CALLBACK}&guid=b6242120-5bce-4b10-9839-d3045a7682da&${path}`;
}

function sanitiseResponse(text) {
  return text.substring(CALLBACK.length + 1, text.length - 1);
}

function fetchRequest(defaultOptions): Function {
  return function request(url, method, body) {
    const options = {
      method,
      body: body && JSON.stringify(body),
      ...defaultOptions
    };

    return fetch(url, options).then(response => {
      switch (response.status) {
        case 400:
        case 401:
        case 403:
        case 404: {
          return response
            .text()
            .then(text => {
              const error = { status: response.status, text: '' };

              if (text) {
                // Remove hash from an error message;
                const textArray = text.split('- ');
                error.text = textArray[1] || textArray[0];
              }

              return Promise.reject(error);
            })
            .catch(error => Promise.reject(error));
        }
        default:
          return response.text().then(text => {
            if (text) {
              try {
                return JSON.parse(sanitiseResponse(text));
              } catch (error) {
                return Promise.reject(error);
              }
            }

            return { status: response.status };
          });
      }
    });
  };
}

// could accept some custom api authentication token param here
export default function api(): { get: Function } {
  // could set other headers here too
  const defaultOptions = {
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  return {
    get: function get(url, body) {
      return fetchRequest(defaultOptions)(url, 'GET', body);
    }
    // put, delete, maybe even XMLHttpRequest (for image/video upload) methods here
  };
}

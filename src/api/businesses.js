// @flow
import { makeApiLink } from './api';
import transformBusinessesResourceResponse from './transformers/businesses';

import type { Api } from 'types/Api';

export function getBusinesses(api: Api) {
  return function(name: string, maxResults: number = 10) {
    return api
      .get(makeApiLink(`name=${name}&maxResults=${maxResults}`))
      .then(transformBusinessesResourceResponse);
  };
}

// @flow
import type { Business, NormalisedMultipleBusinesses } from 'types/Business';

type BusinessList = Array<Business>;

type Response = {
  Message: string,
  Names: BusinessList
};

export default function transformBusinessesResourceResponse(
  response: Response
): NormalisedMultipleBusinesses {
  const { Names } = response;
  const resources = {};

  const listIds = Names.map(
    ({
      Abn,
      AbnStatus,
      IsCurrent,
      Name,
      NameType,
      Postcode,
      Score,
      State
    }: Business) => {
      // resourceMap[Abn] = {
      //   ...business
      // };

      // camelCase the response
      resources[Abn] = {
        abn: Abn,
        abnStatus: AbnStatus,
        isCurrent: IsCurrent,
        name: Name,
        nameType: NameType,
        postCode: Postcode,
        score: Score,
        state: State
      };

      return Abn;
    }
  );

  return {
    listIds,
    resources
  };
}

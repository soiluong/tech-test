import { getBusinesses } from '../businesses';

import {
  mockGetBusinessesApiResponse,
  transformedMockGetBusinessesApiResponse
} from '../../../tests/utils/business';
import { default as mockedApi } from '../../../tests/utils/api';

const mockedResponses = {
  get: mockGetBusinessesApiResponse
};

const mockApi = mockedApi(mockedResponses);

describe('businessesApi integration tests', () => {
  describe('getBusiness', () => {
    it('should return a transformed get business response', () => {
      expect.assertions(1);

      return getBusinesses(mockApi)('url').then(response => {
        expect(response).toMatchObject(transformedMockGetBusinessesApiResponse);
      });
    });
  });
});

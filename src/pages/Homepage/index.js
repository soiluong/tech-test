// @flow
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import GlobalStyle from 'styles/global';

import { searchBusinessesRequest } from 'state/actions/businesses';
import {
  getBusiness,
  getSearchedBusinesses,
  isBusinessSearchListFetching
} from 'state/selectors/businesses';

import SelectedBusiness from 'components/SelectedBusiness';

import {
  HomepageContainer,
  Form,
  Fieldset,
  Input,
  Label,
  SearchResultHeader,
  SearchResultsList,
  SearchResultsListItem,
  SubmitButton
} from './styled';

import type { TransformedBusiness } from 'types/Business';

export default function Homepage() {
  const dispatch = useDispatch();
  const [searchTerm, setSearchTerm] = useState('');
  const [selectedBusinessAbn, setSelectedBusinessAbn] = useState('');

  const searchedBusinesses = useSelector(getSearchedBusinesses);
  const isListFetching = useSelector(isBusinessSearchListFetching);
  const selectedBusiness = useSelector(state =>
    getBusiness(state, selectedBusinessAbn)
  );

  function handleOnChange(e: SyntheticInputEvent<>) {
    setSearchTerm(e.target.value);
  }

  function handleSubmit(e: Event) {
    e.preventDefault();

    searchForTerm();
  }

  function searchForTerm() {
    if (searchTerm) {
      const encodeDescTagerm = encodeURIComponent(searchTerm);
      dispatch(searchBusinessesRequest(encodeDescTagerm));
    } else {
      // clear lists
    }
  }

  function renderListItem(business: TransformedBusiness) {
    if (!business) {
      return null;
    }

    const { abn, name } = business;

    return (
      <SearchResultsListItem
        key={abn}
        tabIndex={0}
        onClick={handleSelectedBusiness(abn)}
        onKeyDown={handleKeyPress(abn)}
      >
        {name}
      </SearchResultsListItem>
    );
  }

  function handleSelectedBusiness(abn: string) {
    return () => {
      setSelectedBusinessAbn(abn);
    };
  }

  function handleKeyPress(abn) {
    return (e: SyntheticKeyboardEvent<>) => {
      if (e.key === 'Enter') {
        handleSelectedBusiness(abn)();
      }
    };
  }

  return (
    <>
      <HomepageContainer>
        <Form onSubmit={handleSubmit}>
          <Fieldset>
            <Label htmlFor="company-name">Search by company name</Label>
            <Input
              id="company-name"
              name="companyName"
              value={searchTerm}
              onChange={handleOnChange}
            />
          </Fieldset>
          <SubmitButton value="Search" />
        </Form>

        {isListFetching ? <span>Searching...</span> : null}

        {searchedBusinesses && searchedBusinesses.length ? (
          <>
            <SearchResultHeader>Search Results</SearchResultHeader>
            <SearchResultsList role="tablist">
              {searchedBusinesses.map(renderListItem)}
            </SearchResultsList>
          </>
        ) : null}

        {!isListFetching &&
        searchedBusinesses &&
        searchedBusinesses.length === 0 ? (
          <span>No businesses found</span>
        ) : null}

        <SelectedBusiness business={selectedBusiness} />
      </HomepageContainer>
      <GlobalStyle />
    </>
  );
}

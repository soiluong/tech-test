import styled, { css } from 'styled-components/macro';

import { cols, media } from 'styles/vars';

export const HomepageContainer = styled.div`
  margin: 0 auto;
  padding-top: 20px;
  max-width: ${cols(30)};
  width: 100%;

  ${media.medium`
    padding: 20px;
  `};
`;

export const Form = styled.form`
  margin-bottom: 25px;
`;

export const Fieldset = styled.fieldset`
  border: none;
  margin: 0;
  padding: 0;
`;

export const Label = styled.label`
  display: block;
  font-weight: bold;
  margin-bottom: 10px;
`;

const inputStyle = css`
  border: 1px solid var(--color-grey);
  border-radius: 4px;
  height: 40px;
  padding: 5px;
`;

export const Input = styled.input.attrs({
  type: 'text'
})`
  ${inputStyle};
  margin-bottom: 10px;
  width: 100%;
`;

export const SubmitButton = styled.input.attrs({
  type: 'submit'
})`
  ${inputStyle};
  background-color: var(--color-grey);
  cursor: pointer;
  padding: 5px 10px;
`;

export const SearchResultsList = styled.ul`
  padding: 0 0 0 20px;
`;

export const SearchResultsListItem = styled.li`
  padding: 10px 0;
`;

export const SearchResultHeader = styled.h2`
  margin: 0 0 10px;
`;
